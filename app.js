// Contenido de chat.js
let video = null;
let detector = null;
let detections = [];
let detecting = false;
let personDetected = false;

let detectionAction = null;
let messageDiv = null;
let videoContainer = null;
let videoAndCanvasContainer = null;

function preload() {
  detector = ml5.objectDetector('cocossd');
}

function setup() {
  detectionAction = document.getElementById('detectionAction');
  messageDiv = document.getElementById('message');
  videoContainer = document.getElementById('videoElement');
  videoAndCanvasContainer = document.getElementById('videoAndCanvasContainer');

  createCanvas(640, 480).parent(videoAndCanvasContainer);

  video = createCapture(VIDEO);
  video.size(640, 480);
  video.parent(videoContainer);
  video.hide();

  document.body.style.cursor = 'default';
}
//
function draw() {
  if (!video || !detecting) return;

  image(video, 0, 0);

  personDetected = false;

  for (let i = 0; i < detections.length; i++) {
    const object = detections[i];
    if (object.label === 'person') {
      personDetected = true;
      drawResult(object);

      const displayMessage = `You are a ${object.label}`;
      messageDiv.innerText = displayMessage;
    }
  }
}

function drawResult(object) {
  boundingBox(object);
  drawLabel(object);
}

function boundingBox(object) {
  stroke('blue');
  strokeWeight(6);
  noFill();
  rect(object.x, object.y, object.width, object.height);
}

function drawLabel(object) {
  noStroke();
  fill('white');
  textSize(34);
  text('Person', object.x + 15, object.y + 34);
}

function onDetected(error, results) {
  if (error) {
    console.error(error);
    return;
  }

  detections = results;

  if (detecting) {
    detect();
  }
}

function detect() {
  detector.detect(video, onDetected);
}

function toggleDetecting() {
  if (!video || !detector) return;
  if (!detecting) {
    detect();
    detectionAction.innerText = 'Parar detección';
  } else {
    detectionAction.innerText = 'Iniciar detección';
  }
  detecting = !detecting;
}

window.onload = function() {
  setup();
};
