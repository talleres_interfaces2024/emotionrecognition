// codigo de chat.js
function enviarMensaje() {
    var mensaje = document.getElementById("textInput").value.trim();
    if (mensaje === "") {
        return;
    }

    mostrarMensajeUsuario(mensaje);
    mostrarMensajeBot("Estoy procesando tu consulta. Por favor, espera un momento...");

    // Realizar la solicitud a la API Llama2 para obtener la respuesta del chatbot en español
    fetch('http://localhost:11434/api/generate', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            model: "llama2",
            prompt: "Quiero que respondas como psicólogo a lo que te voy a decir, en español y menos de 50 palabras: " + mensaje,
            stream: false
        })
    })
    .then(response => response.json())
    .then(data => {
        mostrarMensajeBot(data.response);
    })
    .catch(error => {
        console.error('Error al obtener respuesta del chatbot:', error);
        mostrarMensajeBot("Lo siento, no pude obtener una respuesta en este momento.");
    });

    // Limpiar el campo de texto después de enviar el mensaje
    document.getElementById("textInput").value = "";
}

function mostrarMensajeUsuario(mensaje) {
    var chatMessages = document.getElementById("chat-messages");
    var messageElement = document.createElement("div");
    messageElement.textContent = mensaje;
    messageElement.classList.add("user-message");
    chatMessages.appendChild(messageElement);
}

function mostrarMensajeBot(mensaje) {
    var chatMessages = document.getElementById("chat-messages");
    var messageElement = document.createElement("div");
    messageElement.textContent = mensaje;
    messageElement.classList.add("bot-message");
    chatMessages.appendChild(messageElement);
    chatMessages.scrollTop = chatMessages.scrollHeight; // Desplazar al final del chat
}

function toggleChat() {
    var chatContainer = document.getElementById("chat-container");
    chatContainer.classList.toggle("show");
}

function obtenerMensajeDesdeChat() {
    var mensaje = document.getElementById("textInput").value.trim();
    return mensaje;
}

