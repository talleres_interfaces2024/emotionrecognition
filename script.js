// script.js
document.addEventListener('DOMContentLoaded', function() {
    fetch('emotions.json')
        .then(response => response.json())
        .then(data => {
            window.emociones = data;
        })
        .catch(error => console.error('Error cargando el archivo JSON:', error));
});

function playEmotion() {
    const emocionInput = document.getElementById('emocion-input').value;
    if (emociones.hasOwnProperty(emocionInput)) {
        const videos = emociones[emocionInput];
        const randomIndex = Math.floor(Math.random() * videos.length);
        const videoUrl = videos[randomIndex] + "?autoplay=1";
        document.getElementById('video-player').src = videoUrl;
    } else {
        alert('La emoción ingresada no está en la lista.');
    }
}


// script.js
function toggleDesahogarse() {
    const desahogarseContainer = document.getElementById('desahogarse-container');
    desahogarseContainer.style.display = desahogarseContainer.style.display === 'none' ? 'block' : 'none';
}

function enviarDesahogo() {
    const texto = document.getElementById('desahogarseInput').value;

    if (texto) {
        fetch('/analizar_sentimiento', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ texto: texto })
        })
        .then(response => response.json())
        .then(data => {
            // Mostrar el resultado en el contenedor
            const resultadoDiv = document.getElementById('sentimientoResultado');
            resultadoDiv.innerHTML = `Sentimiento: ${data.sentimiento}<br>Emoción: ${data.emocion}`;
        })
        .catch(error => {
            console.error('Error:', error);
            alert('Ocurrió un error al procesar el texto.');
        });
    } else {
        alert('Por favor, ingresa un texto.');
    }
}
