# # codigo de sentiment_analysis.py
#
# from transformers import pipeline
#
# # Initialize the sentiment analysis pipeline
# clasificador = pipeline('sentiment-analysis', model='nlptown/bert-base-multilingual-uncased-sentiment')
#
#
# def analizar_sentimiento_hf(texto):
#     resultado = clasificador(texto)
#     label = resultado[0]['label']
#
#     # Map sentiment label to emotion
#     if label == '1 star':
#         emotion = 'Sentimiento muy negativo'
#     elif label == '2 stars':
#         emotion = 'Sentimiento negativo'
#     elif label == '3 stars':
#         emotion = 'Sentimiento neutral'
#     elif label == '4 stars':
#         emotion = 'Sentimiento positivo'
#     elif label == '5 stars':
#         emotion = 'Sentimiento muy positivo'
#     else:
#         emotion = 'Sentimiento no identificado'
#
#     return resultado, emotion
#
#
# # Example text for sentiment analysis
# texto = "Me siento muy triste porque perdi mi exámen de ingles el día de hoy y me echaron"
#
# # Perform sentiment analysis on the text
# resultado, emocion = analizar_sentimiento_hf(texto)
# print("Sentimiento:", resultado[0]['label'])
# print("Emoción predominante:", emocion)
#

from flask import Flask, request, jsonify
from transformers import pipeline

# Initialize the sentiment analysis pipeline
clasificador = pipeline('sentiment-analysis', model='nlptown/bert-base-multilingual-uncased-sentiment')

app = Flask(__name__)

@app.route('/analizar_sentimiento', methods=['POST'])
def analizar_sentimiento():
    # Obtener el texto enviado desde el frontend
    texto = request.json['texto']

    # Realizar el análisis de sentimientos
    resultado = clasificador(texto)
    label = resultado[0]['label']

    # Mapear la etiqueta de sentimiento a emoción
    if label == '1 star':
        emocion = 'Sentimiento muy negativo'
    elif label == '2 stars':
        emocion = 'Sentimiento negativo'
    elif label == '3 stars':
        emocion = 'Sentimiento neutral'
    elif label == '4 stars':
        emocion = 'Sentimiento positivo'
    elif label == '5 stars':
        emocion = 'Sentimiento muy positivo'
    else:
        emocion = 'Sentimiento no identificado'

    # Devolver el resultado del análisis de sentimientos al frontend
    return jsonify({'sentimiento': label, 'emocion': emocion})

if __name__ == '__main__':
    app.run(debug=True)
